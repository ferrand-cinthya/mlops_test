# Predict'Immo

> Estimez la valeur de votre bien immobilier

L'API REST Predict'Immo permet d'estimer très simplement la valeur foncière
d'un bien immobilier en France.

## Entrainement

Le notebook d'entrainement du modèle : [training.ipynb](training.ipynb)

Le modèle est sérialisé dans le fichier [model.joblib](model.joblib).

## Développement

Pour lancer l'API REST en local : `flask run --debug`

Pour exécuter les tests automatiques : `pytest -v tests`

## Production

Pour construire l'image Docker : `docker build -t predictimmo:latest .`

Pour créer un conteneur avec l'image Docker : `docker run --rm -p "5000:5000" predictimmo:latest`

## Usage

Pour effectuer une prédiction :

```bash
curl -X POST http://localhost:5000 \
   -H 'Content-Type: application/json' \
   -d '{"housing_type": "appartment", "surface": 50, "rooms": 3}'
```
